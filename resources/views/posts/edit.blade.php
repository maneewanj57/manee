@extends('layouts.app')
@section('content')

<div class="row">
  <div class="col-sm-12">
    <div class="full-right">
     <center> <br><h2>CRUD BTC</h2></center>
     <br>
    
  
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      {{ Form::model($post,['route'=>['posts.update',$post->id],'method'=>'PATCH']) }}
      @include('posts.form_master')
      {{ Form::close() }}
    </div>
  </div>
  </div>
  </div>
  </div>
@endsection