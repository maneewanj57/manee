<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $fillable = ['title','body'];
    protected $datas = ['created_at','updated_at'];
}
